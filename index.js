const myHeader = require("./app/header-component");
const Shell = require("./app/shell");

exports.decorateConfig = (cfg) => {
  if (this.shell) {
    //It makes sure that the shells are always bound correctly
    this.shell.bindShellToConfig(cfg);
  } else {
    // If there isn't a shell defined yet, start the plugin up
    this.shell = new Shell(cfg);
  }
  return cfg;
};

exports.decorateEnv = (env) => {
  let newEnv = env;
  if (this.shell && this.shell.currentShell) {
    if (this.shell.currentShell.env) {
      newEnv = Object.assign(env, this.shell.currentShell.env);
    }
    // This relies on the fact that env is decorated after shell and shellArgs are obtained.
    // If that fact changes, this could break.
    // But this is probably the simplest way to implement it
    if (this.shell.currentShell.revertTo) {
      this.shell.setShell(this.shell.currentShell.revertTo);
      delete this.shell.currentShell.revertTo;
    } 
  } 
  return newEnv;
}

exports.decorateMenu = (menu) =>  {
    menu.push({type: 'separator'});
    menu.push({label: 'Shells', submenu: this.shell.menu});
    return menu;
}

exports.onWindow = (window) => {
  // options = {window, x, y, positioningItem, callback}
  window.rpc.on('open launch menu', (options = {}) => {
    this.shell.menu.popup(options);
  });
}

exports.decorateHeader = (Header, {React, notify}) => {
  // It allows the plugin to send notifications to the user
  rpc.on('notify', ({title, body, details}) => notify(title, body, details));
  
  return myHeader(Header, React);
}
